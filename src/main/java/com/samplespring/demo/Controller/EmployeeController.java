package com.samplespring.demo.Controller;

import com.samplespring.demo.Repository.EmployeeRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.samplespring.demo.Model.Employee;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
    
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee employee){
         return employeeRepository.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployees(@PathVariable(value="id") Long employeeId, @Valid @RequestBody Employee employeeData){
        
        var employee = employeeRepository.getById((employeeId));

        employee.setEmailId(employeeData.getEmailId());
        employee.setFirstName(employeeData.getFirstName());
        employee.setLastName(employeeData.getFirstName());

        var udpatedEmp = employeeRepository.save(employee);
        
        return ResponseEntity.ok(udpatedEmp);
    }

    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value="id") Long id){

        Employee emp = employeeRepository.getById((id));
         employeeRepository.delete(emp);
         Map<String, Boolean> result = new HashMap<>();
         result.put("deleted", true);
         return result;
    }
}
