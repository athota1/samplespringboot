package com.samplespring.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {
    
    @GetMapping("/")
    public String Get(){
        return "Hello World from Spring1";
    }
}
